import React from 'react'
import { BrowserRouter as Router, Switch, Redirect, Route } from 'react-router-dom'
import Home from './Pages/Home/Home'
import Diy from './Pages/Diy/Diy'
import Question from './Pages/Question/Question'
import Users from './Pages/Users/Users'
import User from './Pages/User/User'
import './App.scss'

function App() {
    return (
        <Router>
            <Switch>
                <Route path='/home' component={Home}></Route>
                <Route path='/diy' component={Diy}></Route>
                <Route path='/question' component={Question}></Route>
                <Route path='/users' component={Users}></Route>
                <Route path='/user' component={User}></Route>
                <Redirect to='/home'></Redirect>
            </Switch>
        </Router>
    )
}

export default App
