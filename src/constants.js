const users = [
    {
        name: 'Amir Naderi',
        details: 'New Commer',
        avatar: `${process.env.PUBLIC_URL}/images/users/amir.jpg`,
        certified: false,
        score: 1245
    },
    {
        name: 'Arash Zolanvari',
        details: 'Power-saw Enthusiast',
        avatar: `${process.env.PUBLIC_URL}/images/users/arash.jpg`,
        certified: true,
        score: 1535
    },
    {
        name: 'Don MHMMD',
        details: 'Hard Worker',
        avatar: `${process.env.PUBLIC_URL}/images/users/mhmmd.jpg`,
        certified: false,
        score: 849
    },
    {
        name: 'M.Hossein Malmir',
        details: 'Certified plumber',
        avatar: `${process.env.PUBLIC_URL}/images/users/mhossein.jpg`,
        certified: false,
        score: 921
    },
    {
        name: 'Peyman Yousefi',
        details: 'DIYer',
        avatar: `${process.env.PUBLIC_URL}/images/users/peyman.jpg`,
        certified: false,
        score: 1028
    },
    {
        name: 'Susie Wright',
        details: 'Lightning Master',
        avatar: `${process.env.PUBLIC_URL}/images/users/person1.png`,
        certified: true,
        score: 129
    },
    {
        name: 'Nora Howe',
        details: 'Drilling NOOB',
        avatar: `${process.env.PUBLIC_URL}/images/users/person2.png`,
        certified: false,
        score: 482
    },
    {
        name: 'Sarah Ford',
        details: 'Hammer Master',
        avatar: `${process.env.PUBLIC_URL}/images/users/person3.png`,
        certified: false,
        score: 309
    },
    {
        name: 'Lacey Bravo',
        details: 'New Commer',
        avatar: `${process.env.PUBLIC_URL}/images/users/person4.png`,
        certified: false,
        score: 1096
    }
]

const diys = [
    {
        image: `${process.env.PUBLIC_URL}/images/diy/14.jpg`,
        title: 'CREATE! DIY wall clock'
    },
    {
        image: `${process.env.PUBLIC_URL}/images/diy/15.jpg`,
        title: 'CREATE! DIY copper vaseKnauf Urbanscape @Home DIY green roof system complete set'
    },
    {
        image: `${process.env.PUBLIC_URL}/images/diy/8.jpg`,
        title: 'CREATE! DIY cube lamp EEK: A +'
    },
    {
        image: `${process.env.PUBLIC_URL}/images/diy/12.jpg`,
        title: 'CREATE! DIY table lamp small EEK: A +'
    },
    {
        image: `${process.env.PUBLIC_URL}/images/diy/7.jpg`,
        title: 'CREATE! DIY candlestick'
    },
    {
        image: `${process.env.PUBLIC_URL}/images/diy/4.jpg`,
        title: 'CREATE! DIY planting bench'
    }
]

const products = [
    { title: `Bosch PMF 220 CE / PMF`, image: `${process.env.PUBLIC_URL}/images/tools/1.jpg` },
    { title: 'LUX-TOOLS polishing machine C-APM-1200', image: `${process.env.PUBLIC_URL}/images/tools/4.jpg` },
    { title: 'Mini tool set 100 pieces', image: `${process.env.PUBLIC_URL}/images/tools/3.jpg` },
    { title: 'LUX-TOOLS electric leaf blower E-LS-3000/45', image: `${process.env.PUBLIC_URL}/images/tools/10.jpg` },
    { title: 'Wera Bit-Set Tool-Check Plus PH PZ 39 pieces', image: `${process.env.PUBLIC_URL}/images/tools/5.jpg` },
    {
        title: 'Bosch Professional cordless drill GSR 18 V-60 FC Baretool',
        image: `${process.env.PUBLIC_URL}/images/tools/11.jpg`
    },
    {
        title: 'kwb battery top multitool accessory set 7 pieces in aluminum case',
        image: `${process.env.PUBLIC_URL}/images/tools/8.jpg`
    },
    { title: 'Systainer® 1 from LUX-TOOLS', image: `${process.env.PUBLIC_URL}/images/tools/12.jpg` }
]

const messages = [
    `What is the best color set for using in kids' room?`,
    `Is there any way to repair the lock of this box?`,
    `If you like, choose your desired color directly. You will get the right CREATE! DIY color set separately. We have selected a few colors for you that you might like.`,
    `Of course you have to create your CREATE! Do not give away the DIY set. You can also get started straight away. And if you're really proud of your decoration, just share your photo under the hashtag #deinoriginal.`,
    `The material makes the product: copper reflects surrounding colors and therefore fits almost anywhere.`,
    `The DIY planting bench is THE gift idea - in every season.`,
    `Simple and beautiful design - very easy to assemble and install`,
    `Looks very good. could be set up quickly and easily. The light bulb sits a bit wobbly but does not bother you if the lamp has a fixed place`,
    `The lamp is easy to assemble and makes a great impression. Everything you need is included in the package.`
]

export { users, messages, diys, products }
