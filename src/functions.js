import { LoremIpsum } from 'lorem-ipsum'

let toClassName = classes => classes.filter(c => c).join(' ')

let formatTimestamp = t => {
    let d = new Date(t)
    return `${d.getHours()}:${d.getMinutes()} ${d.toDateString().split(' ')[1]} ${d.getDate()}`
}

const lorem = new LoremIpsum({
    sentencesPerParagraph: {
        max: 2,
        min: 1
    },
    wordsPerSentence: {
        max: 16,
        min: 4
    }
})

export { toClassName, formatTimestamp, lorem }
