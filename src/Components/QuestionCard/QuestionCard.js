import React from 'react'
import { sentenceCase } from 'change-case'
import { toClassName } from '../../functions'
import './QuestionCard.scss'

function QuestionCard({ className, onClick, title, content, author }) {
    let { name, avatar, details, certified } = author || {}
    return (
        <div className={toClassName(['question-card', className])} onClick={onClick}>
            <span className='title'>{title}</span>
            <p className='question-content'>{content}</p>
            <div className='author-container'>
                <img className='author-avatar' src={avatar} alt=''></img>
                <div className='author-title'>
                    <span className='author-name'>
                        {sentenceCase(name)}
                        {certified && <span className='certified fas fa-badge-check'></span>}
                    </span>
                    <span className='author-details'>{details}</span>
                </div>
                <span style={{ flex: '1' }}></span>
                <span className='action far fa-bookmark fa-fw'></span>
                <span className='action far fa-heart fa-fw'></span>
            </div>
        </div>
    )
}

export default QuestionCard
