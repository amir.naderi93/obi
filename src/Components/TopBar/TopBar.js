import React from 'react'
import { toClassName } from '../../functions'
import './TopBar.scss'

function TopBar({ openMenu, profile }) {
    return (
        <div className='top-bar-container'>
            <div className={toClassName(['top-bar', profile && 'profile'])}>
                {openMenu ? (
                    <span className='top-bar-button far fa-bars fa-fw' onClick={openMenu}></span>
                ) : (
                    <span
                        className='top-bar-button far fa-chevron-left fa-fw'
                        onClick={() => window.history.back()}
                    ></span>
                )}

                <span style={{ display: 'flex', flex: '1' }}></span>
                <span className='top-bar-button far fa-bell fa-fw'></span>
                <span
                    className='top-bar-button far fa-trophy-alt fa-fw'
                    onClick={() => (window.location = '/users')}
                ></span>
                <span className='top-bar-button far fa-shopping-cart fa-fw'></span>
            </div>
            {profile && (
                <div className='user-container'>
                    <img src={`${process.env.PUBLIC_URL}/images/users/arash.jpg`} alt='arash'></img>
                    <span className='user-name'>
                        Arash<span className='certified fas fa-badge-check'></span>
                    </span>
                    <span className='user-details'>Power-saw Enthusiast</span>
                </div>
            )}
        </div>
    )
}

export default TopBar
