import React from 'react'
import { toClassName } from '../../functions'
import './StepCard.scss'

function StepCard({ className, onClickAR, title, img, children }) {
    return (
        <div className={toClassName(['step-card', className])}>
            <div className='media-container'>
                <img className='main-image' src={img} alt={title}></img>
                <span className='media-expand fa-stack fa-fw' onClick={onClickAR}>
                    <i className='fal fa-stack-2x fa-expand'></i>
                    <i className='far fa-stack-1x fa-cube'></i>
                </span>
            </div>
            <div className='content-container'>
                <p>{children}</p>
            </div>
        </div>
    )
}

export default StepCard
