import React from 'react'
import './BottomBar.scss'

function BottomBar({ page = null }) {
    return (
        <div className='bottom-bar'>
            <span className='bottom-bar-button far fa-home-alt' onClick={() => (window.location = '/home')}></span>
            <span className='bottom-bar-button far fa-search'></span>
            <span className='bottom-bar-button far fa-plus' style={{ padding: '0 10px', fontSize: '4rem' }}></span>
            <span className='bottom-bar-button far fa-bookmark'></span>
            <span className='bottom-bar-button far fa-headset'></span>
        </div>
    )
}

export default BottomBar
