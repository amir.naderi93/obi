import React from 'react'
import { sentenceCase } from 'change-case'
import { toClassName } from '../../functions'
import './DiyCard.scss'

function DiyCard({ className, onClick, title, img, author }) {
    let { name, avatar, details, certified } = author || {}
    return (
        <div className={toClassName(['diy-card', className])} onClick={onClick}>
            <div className='media-container'>
                <img className='main-image' src={img} alt={title}></img>
            </div>
            <span className='title'>{title}</span>
            <div className='author-container'>
                <img className='author-avatar' src={avatar} alt=''></img>
                <div className='author-title'>
                    <span className='author-name'>
                        {sentenceCase(name || '')}
                        {certified && <span className='certified fas fa-badge-check'></span>}
                    </span>
                    <span className='author-details'>{details}</span>
                </div>
                <span style={{ flex: '1' }}></span>
                <span className='action far fa-bookmark fa-fw'></span>
                <span className='action far fa-heart fa-fw'></span>
            </div>
        </div>
    )
}

export default DiyCard
