import React, { useState, useEffect, Fragment } from 'react'
import { SwitchTransition, CSSTransition } from 'react-transition-group'
import Webcam from 'react-webcam'
import './Diy.scss'
import { formatTimestamp } from '../../functions'
import TopBar from '../../Components/TopBar/TopBar'
import BottomBar from '../../Components/BottomBar/BottomBar'
import DiyCard from '../../Components/DiyCard/DiyCard'
import StepCard from '../../Components/StepCard/StepCard'
import { users, messages, diys, products } from '../../constants'

function Diy() {
    const [webcam, setWebcam] = useState(false)
    const [stepState, setStepState] = useState({ step: 0, dir: 1 })
    const [bookmark, setBookmark] = useState(false)
    const [vote, setVote] = useState(0)
    const [stepChanged, setStepChanged] = useState(false)
    const [stepLabel, setStepLabel] = useState(1)
    const [sort, setSort] = useState(-1)
    useEffect(() => {
        if (stepState.step) {
            setStepChanged(s => !s)
            setStepLabel(stepState.step + 1)
        }
    }, [stepState.step])

    const steps = [
        {
            media: '1.jpg',
            content: `The green roof system consists of a complete solution with all the necessary components that are easy to install. The special feature here is that the 1 square meter growth mat is extremely light, but can permanently store a lot of water. With a thickness of 2 cm, it absorbs up to 17 liters of rainwater. Evaporation also creates optimal heat regulation. The patented growth mat has good fire resistance and is a sustainable solution for green roofs, thanks to the natural, binder-free rock wool mat.`
        },
        {
            media: '2.jpg',
            content: `The construction and installation of this green roof system is extremely straightforward, because the growth mats can be installed particularly quickly and practically. The structure for your long-lasting green roof consists of four different layers. The root protection layer seals the roof surface and protects against root penetration. The second layer is the drainage with water storage, which serves as a ventilation system and protects against rotting. The growth mat is then laid and ensures that water is available in dry periods. On top is the planted vegetation layer, which consists of 8-10 different types of sedum, which efficiently store water in the leaves.`
        },
        {
            media: '3.jpg',
            content: `The green roof system consists of a complete solution with all the necessary components that are easy to install. The special feature here is that the 1 square meter growth mat is extremely light, but can permanently store a lot of water. With a thickness of 2 cm, it absorbs up to 17 liters of rainwater. Evaporation also creates optimal heat regulation. The patented growth mat has good fire resistance and is a sustainable solution for green roofs, thanks to the natural, binder-free rock wool mat.`
        },
        {
            media: '4.jpg',
            content: `The construction and installation of this green roof system is extremely straightforward, because the growth mats can be installed particularly quickly and practically. The structure for your long-lasting green roof consists of four different layers. The root protection layer seals the roof surface and protects against root penetration. The second layer is the drainage with water storage, which serves as a ventilation system and protects against rotting. The growth mat is then laid and ensures that water is available in dry periods. On top is the planted vegetation layer, which consists of 8-10 different types of sedum, which efficiently store water in the leaves.`
        },
        {
            media: '5.jpg',
            content: `The green roof system consists of a complete solution with all the necessary components that are easy to install. The special feature here is that the 1 square meter growth mat is extremely light, but can permanently store a lot of water. With a thickness of 2 cm, it absorbs up to 17 liters of rainwater. Evaporation also creates optimal heat regulation. The patented growth mat has good fire resistance and is a sustainable solution for green roofs, thanks to the natural, binder-free rock wool mat.`
        }
    ]
    const similarProjects = [
        {
            media: '11.jpg',
            content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`
        },
        {
            media: '10.jpg',
            content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`
        },
        {
            media: '9.jpg',
            content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`
        },
        {
            media: '8.jpg',
            content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`
        },
        {
            media: '7.jpg',
            content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`
        },
        {
            media: '6.jpg',
            content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`
        }
    ]

    const comments = users.map(({ name, details, avatar, certified }, i) => ({
        timestamp: 1590314955216 - 127634 * (i + 5),
        name,
        details,
        avatar,
        certified,
        message: messages[i]
    }))

    return (
        <div className='page diy'>
            {webcam ? (
                <Fragment>
                    <Webcam
                        width={375}
                        height={667}
                        videoConstraints={{
                            width: 375,
                            height: 667,
                            facingMode: 'user'
                        }}
                    ></Webcam>
                    <span
                        className='cta-button back far fa-chevron-left fa-fw'
                        style={{
                            color: 'black',
                            background: '#00000040'
                        }}
                        onClick={() => setWebcam(false)}
                    ></span>
                </Fragment>
            ) : (
                <Fragment>
                    <TopBar />
                    <div className='main'>
                        <div className='image-container'>
                            <img src={`${process.env.PUBLIC_URL}/images/diy/15.jpg`} alt=''></img>
                            <SwitchTransition mode='in-out'>
                                <CSSTransition key={bookmark} classNames='pulse' timeout={200}>
                                    <span
                                        className={['cta-button bookmark far fa-bookmark fa-fw', bookmark && 'selected']
                                            .filter(c => c)
                                            .join(' ')}
                                        onClick={() => setBookmark(!bookmark)}
                                    ></span>
                                </CSSTransition>
                            </SwitchTransition>

                            <span className='cta-button ar fa-stack fa-fw' onClick={() => setWebcam(true)}>
                                <i className='fal fa-stack-2x fa-expand'></i>
                                <i className='far fa-stack-1x fa-cube'></i>
                            </span>
                        </div>
                        <div className='badge-container'>
                            <div className='badge'>
                                <span className='badge-icon far fa-wrench'></span>
                                <span className='badge-content'>MEDIUM</span>
                            </div>
                            <div className='badge'>
                                <span className='badge-icon far fa-user'></span>
                                <span className='badge-content'>2</span>
                            </div>
                            <div className='badge'>
                                <span className='badge-icon far fa-euro-sign'></span>
                                <span className='badge-content'>100</span>
                            </div>
                            <div className='badge'>
                                <span className='badge-icon far fa-stopwatch'></span>
                                <span className='badge-content'>2 HOURS</span>
                            </div>
                        </div>
                        <div className='header'>
                            <div className='score-container'>
                                <span
                                    className={['score far fa-chevron-up', vote === 1 && 'selected']
                                        .filter(c => c)
                                        .join(' ')}
                                    onClick={() => {
                                        if (vote === 1) setVote(0)
                                        else setVote(1)
                                    }}
                                ></span>
                                <SwitchTransition>
                                    <CSSTransition key={vote} classNames='fade' timeout={200}>
                                        <span className='score-value'>{24 + vote}</span>
                                    </CSSTransition>
                                </SwitchTransition>
                                <span
                                    className={['score far fa-chevron-down', vote === -1 && 'selected']
                                        .filter(c => c)
                                        .join(' ')}
                                    onClick={() => {
                                        if (vote === -1) setVote(0)
                                        else setVote(-1)
                                    }}
                                ></span>
                            </div>
                            <h1>Knauf Urbanscape @Home DIY green roof system complete set</h1>
                        </div>
                        <p className='brief'>
                            With the Knauf Urbanscape @Home green roof system, you can transform your building into an
                            oasis with sustainable green roofs and a natural look. You can green your house roof, garden
                            shed or garage.
                            <ul>
                                <li>Light DIY complete set for sure success</li>
                                <li>Easy and quick installation without special tools</li>
                                <li>High water absorption and optimal thermal performance of the green roof</li>
                                <li>Light and at the same time high noise and sound insulation</li>
                            </ul>
                        </p>
                        <h2 className='tools-title'>WHAT YOU NEED</h2>
                        <div className='tools-container'>
                            <div className='tools-scroll'>
                                {products.map(({ title, image }, i) => (
                                    <div key={i} className='tool'>
                                        <img src={image} alt=''></img>
                                        <div className='tool-titlebar'>
                                            <span className='tool-title'>{title}</span>
                                            <span className='tool-price'>
                                                <span className='main-part'>{`${
                                                    i % 2 ? 153 + 17 * i : 196 + 15 * i
                                                },`}</span>
                                                <span className='change-part'>99</span>
                                            </span>
                                        </div>
                                        <div className='tool-actionbar'>
                                            <span className='action far fa-shopping-cart'></span>
                                        </div>
                                    </div>
                                ))}
                            </div>
                        </div>
                        <div className='buy-all-container'>
                            <span className='buy-all'>
                                <span className='buy-all-icon far fa-shopping-cart'></span>
                                <span className='buy-all-content'>ORDER ALL</span>
                            </span>
                        </div>
                        <h2 className='content-title'>INSTRUCTIONS</h2>
                        <div className='steps-container'>
                            <div
                                className='steps-scroll'
                                style={{
                                    left: stepState.step ? `calc(${stepState.step} * calc(40px - 100vw) + 10px)` : '0'
                                }}
                            >
                                {steps.map(({ media, content }, i) => (
                                    <StepCard
                                        key={i}
                                        img={`${process.env.PUBLIC_URL}/images/steps/${media}`}
                                        onClickAR={() => setWebcam(true)}
                                    >
                                        {content}
                                    </StepCard>
                                ))}
                            </div>
                        </div>
                        <div className='steps-nav'>
                            <span
                                className={[
                                    'step-nav-button far fa-chevron-left',
                                    stepState.step <= 0 && 'disabled'
                                ].join(' ')}
                                onClick={() => {
                                    if (stepState.step > 0) {
                                        setStepState({ step: stepState.step - 1, dir: -1 })
                                    }
                                }}
                            ></span>
                            <SwitchTransition>
                                <CSSTransition
                                    key={stepChanged}
                                    classNames={stepState.dir === 1 ? 'wipe-to-left' : 'wipe-to-right'}
                                    timeout={500}
                                >
                                    <span className='step-value'>{`STEP ${stepLabel}`}</span>
                                </CSSTransition>
                            </SwitchTransition>
                            <span
                                className={[
                                    'step-nav-button far fa-chevron-right',
                                    stepState.step >= steps.length - 1 && 'disabled'
                                ].join(' ')}
                                onClick={() => {
                                    if (stepState.step < steps.length - 1) {
                                        setStepState({ step: stepState.step + 1, dir: 1 })
                                    }
                                }}
                            ></span>
                        </div>
                        <div className='comments-title-container'>
                            <h2 className='content-title'>COMMENTS</h2>
                            <span
                                className={[
                                    'comments-sort-button far',
                                    sort === 1
                                        ? 'fa-sort-amount-up'
                                        : sort === -1
                                        ? 'fa-sort-amount-down'
                                        : 'fa-sort-alt'
                                ]
                                    .filter(c => c)
                                    .join(' ')}
                                onClick={() => setSort(s => (s === 1 ? -1 : 1))}
                            ></span>
                        </div>
                        <div className='comments-container'>
                            {comments
                                .slice(0)
                                .sort((a, b) => sort * (a.timestamp - b.timestamp))
                                .map(({ timestamp, name, details, avatar, certified, message }, i) => (
                                    <div key={i} className='comment-container'>
                                        <div className='author-container'>
                                            <img className='author-avatar' src={avatar} alt={name}></img>
                                            <div className='author-title'>
                                                <span className='author-name'>
                                                    {name}
                                                    {certified && (
                                                        <span className='certified fas fa-badge-check'></span>
                                                    )}
                                                </span>
                                                <span className='author-details'>{details}</span>
                                            </div>
                                            <span className='timestamp'>{formatTimestamp(timestamp)}</span>
                                        </div>
                                        <p className='comment-content'>{message}</p>
                                    </div>
                                ))}
                        </div>
                        <h2 className='content-title'>SIMILAR PROJECTS</h2>
                        <div className='steps-container' style={{ overflow: 'auto hidden' }}>
                            <div className='steps-scroll'>
                                {similarProjects.map(({ media, content }, i) => (
                                    <DiyCard
                                        key={i}
                                        className='similar-project'
                                        title={diys[i].title}
                                        img={`${process.env.PUBLIC_URL}/images/diy/${media}`}
                                        onClick={() => (window.location = '/diy')}
                                        author={users[8 - i]}
                                    >
                                        <p>{content}</p>
                                    </DiyCard>
                                ))}
                            </div>
                        </div>
                    </div>
                    <BottomBar />
                </Fragment>
            )}
        </div>
    )
}

export default Diy
