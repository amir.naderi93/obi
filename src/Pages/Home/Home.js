import React, { useState } from 'react'
import TopBar from '../../Components/TopBar/TopBar'
import BottomBar from '../../Components/BottomBar/BottomBar'
import DiyCard from '../../Components/DiyCard/DiyCard'
import QuestionCard from '../../Components/QuestionCard/QuestionCard'
import './Home.scss'
import { users, diys, messages } from '../../constants'

function Home() {
    const [menuOpen, setMenuOpen] = useState(false)
    return (
        <div className='page home'>
            <TopBar openMenu={() => setMenuOpen(true)} />
            <div className='main'>
                <h2 className='content-title'>RECENT DIYs</h2>
                <div className='recent-diys steps-container' style={{ overflow: 'auto hidden' }}>
                    <div className='recent-diys steps-scroll'>
                        {diys.map(({ title, image }, i) => (
                            <DiyCard
                                key={i}
                                className='recent-diy'
                                title={title}
                                img={image}
                                author={users[i]}
                                onClick={() => (window.location = '/diy')}
                            ></DiyCard>
                        ))}
                    </div>
                </div>
                <h2 className='content-title'>HOT QUESTIONS</h2>
                <div className='hot-questions steps-container' style={{ overflow: 'auto hidden' }}>
                    <div className='hot-questions steps-scroll'>
                        {[...Array(4).keys()].map(i => (
                            <QuestionCard
                                key={i}
                                className='hot-question'
                                title={
                                    i === 0
                                        ? `A problem with some DIY project`
                                        : (messages[i] || '').split(' ').slice(0, 3).join(' ')
                                }
                                content={messages[i]}
                                img={`${process.env.PUBLIC_URL}/images/diy/${i + 3}.jpg`}
                                author={users[7 - i]}
                                onClick={() => (window.location = '/question')}
                            ></QuestionCard>
                        ))}
                    </div>
                </div>
                <h2 className='content-title'>TAILORED FOR YOU</h2>
                <div className='tailoreds steps-container' style={{ overflow: 'auto hidden' }}>
                    <div className='tailoreds steps-scroll'>
                        {diys
                            .slice(3)
                            .reverse()
                            .map(({ title, image }, i) => (
                                <DiyCard
                                    key={i}
                                    className='recent-diy'
                                    title={title}
                                    img={image}
                                    author={users[2 * i + 1]}
                                    onClick={() => (window.location = '/diy')}
                                ></DiyCard>
                            ))}
                    </div>
                </div>
            </div>
            <BottomBar />
            <div className={['side-menu-container', menuOpen && 'open'].filter(c => c).join(' ')}>
                <div className='side-menu'>
                    <div className='close-menu-button-container'>
                        <span className='close-menu-button far fa-times' onClick={() => setMenuOpen(false)}></span>
                    </div>
                    <ul className='menu-items'>
                        <li className='menu-item' onClick={() => (window.location = '/user')}>
                            <span className='menu-item-icon far fa-user fa-fw'></span>
                            <span className='menu-item-text'>PROFILE</span>
                        </li>
                        <li className='menu-item'>
                            <span className='menu-item-icon far fa-cog fa-fw'></span>
                            <span className='menu-item-text'>SETTINGS</span>
                        </li>
                        <li className='menu-item'>
                            <span className='menu-item-icon far fa-headset fa-fw'></span>
                            <span className='menu-item-text'>SUPPORT</span>
                        </li>
                        <li className='menu-item'>
                            <span className='menu-item-icon far fa-info-circle fa-fw'></span>
                            <span className='menu-item-text'>ABOUT</span>
                        </li>
                        <li className='menu-item'>
                            <span className='menu-item-icon far fa-sign-out fa-fw'></span>
                            <span className='menu-item-text'>LOGOUT</span>
                        </li>
                    </ul>
                </div>
                <div className='side-menu-mask' onClick={() => setMenuOpen(false)}></div>
            </div>
        </div>
    )
}

export default Home
