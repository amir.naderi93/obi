import React, { useState } from 'react'
import { SwitchTransition, CSSTransition } from 'react-transition-group'
import { sentenceCase } from 'change-case'
import './Question.scss'
import { formatTimestamp, lorem } from '../../functions'
import BottomBar from '../../Components/BottomBar/BottomBar'
import TopBar from '../../Components/TopBar/TopBar'
import { users, messages } from '../../constants'

function Question() {
    const [vote, setVote] = useState(0)
    const [answerVote, setAnswerVote] = useState({})
    const [sort, setSort] = useState(-1)

    const answers = users.map(({ name, details, avatar, certified }, i) => ({
        id: i + 1,
        timestamp: 1590314955216 - 127634 * (i + 5),
        name,
        details,
        avatar,
        certified,
        message: messages[i]
    }))

    return (
        <div className='page question'>
            <TopBar />
            <div className='main'>
                <div className='header'>
                    <div className='score-container'>
                        <span
                            className={['score far fa-chevron-up', vote === 1 && 'selected'].filter(c => c).join(' ')}
                            onClick={() => {
                                if (vote === 1) setVote(0)
                                else setVote(1)
                            }}
                        ></span>
                        <SwitchTransition>
                            <CSSTransition key={vote} classNames='fade' timeout={200}>
                                <span className='score-value'>{3 + vote}</span>
                            </CSSTransition>
                        </SwitchTransition>
                        <span
                            className={['score far fa-chevron-down', vote === -1 && 'selected']
                                .filter(c => c)
                                .join(' ')}
                            onClick={() => {
                                if (vote === -1) setVote(0)
                                else setVote(-1)
                            }}
                        ></span>
                    </div>
                    <h1>A problem with some DIY project</h1>
                </div>
                <div className='question-content'>
                    <p>
                        Versatile and compact tool With the tool included in the set, you can react flexibly to a wide
                        variety of problems at any time. Even if conventional machine or hand tools cannot be used, you
                        can still work comfortably and quickly with the mini ratchet. With the switch lever, you can
                        switch from left to right and vice versa in a matter of seconds, which means maximum efficiency.
                        Ergonomic tool In stressful and strenuous situations, it is important that your tool convinces
                        with comfort. The Kraftform Plus grip nestles into your hands with its soft grip zones and
                        enables constant, reliable torque transmission thanks to its secure hold. Even with slightly
                        oily hands, you have everything under control. The bits are picked up directly by the ratchet
                        head of the bit ratchet, which makes your work at low heights easier. The Rapidaptor bit holder
                        makes it possible to use it with one hand and has a quick-turning sleeve. Mini ratchet with low
                        return angle In tight installation cases , the low return angle of 6 ° comes into play, which
                        also enables particularly precise work.
                    </p>
                    <img className='question-image' src={`${process.env.PUBLIC_URL}/images/diy/8.jpg`} alt=''></img>
                    <p>
                        <ul>
                            <li>Small return angle of 6 ° for precise work</li>
                            <li>Robust , drop- forged, all-steel design</li>
                            <li>Tough bits for universal use</li>
                            <li>Hand and machine nuts for almost all applications</li>
                            <li>Handy for easy gripping even with oily hands</li>
                            <li>
                                With ball catch groove - For hexagon socket screws and Nuts - Made of chrome-plated
                                chrome vanadium steel
                            </li>
                        </ul>
                    </p>
                    <img className='question-image' src={`${process.env.PUBLIC_URL}/images/diy/10.jpg`} alt=''></img>
                    <p>
                        In order not to have to spend time finding your tool, they are color-coded. So you can find the
                        tool you need at a glance. At the same time, the tool consists of matt chrome-plated chrome
                        vanadium steel, which makes high torques possible without any problems.
                    </p>
                </div>
                <div className='answers-title-container'>
                    <h2 className='content-title'>ANSWERS</h2>
                    <span
                        className={[
                            'answers-sort-button far',
                            sort === 1 ? 'fa-sort-amount-up' : sort === -1 ? 'fa-sort-amount-down' : 'fa-sort-alt'
                        ]
                            .filter(c => c)
                            .join(' ')}
                        onClick={() => setSort(s => (s === 1 ? -1 : 1))}
                    ></span>
                </div>
                <div className='answers-container'>
                    {answers
                        .slice(0)
                        .sort((a, b) => sort * (a.timestamp - b.timestamp))
                        .map(({ id, timestamp, name, details, avatar, certified, message }, i) => (
                            <div key={i} className='answer-container'>
                                <div className='author-container'>
                                    <div
                                        className={['answer-score-container', id === 1 && 'accepted']
                                            .filter(c => c)
                                            .join(' ')}
                                    >
                                        <span
                                            className={[
                                                'score far fa-chevron-up',
                                                answerVote[id] && answerVote[id] === 1 && 'selected'
                                            ]
                                                .filter(c => c)
                                                .join(' ')}
                                            onClick={() => {
                                                if (answerVote[id] && answerVote[id] === 1)
                                                    setAnswerVote(v => ({ ...v, [id]: 0 }))
                                                else setAnswerVote(v => ({ ...v, [id]: 1 }))
                                            }}
                                        ></span>
                                        <SwitchTransition>
                                            <CSSTransition
                                                key={Math.floor(answerVote[id] || 0)}
                                                classNames='fade'
                                                timeout={200}
                                            >
                                                <span className='score-value'>
                                                    {(Math.floor(timestamp / 1000) % 7) + (answerVote[id] || 0)}
                                                </span>
                                            </CSSTransition>
                                        </SwitchTransition>
                                        <span
                                            className={[
                                                'score far fa-chevron-down',
                                                answerVote[id] && answerVote[id] === -1 && 'selected'
                                            ]
                                                .filter(c => c)
                                                .join(' ')}
                                            onClick={() => {
                                                if (answerVote[id] && answerVote[id] === -1)
                                                    setAnswerVote(v => ({ ...v, [id]: 0 }))
                                                else setAnswerVote(v => ({ ...v, [id]: -1 }))
                                            }}
                                        ></span>
                                    </div>
                                    <img className='author-avatar' src={avatar} alt={name}></img>
                                    <div className='author-title'>
                                        <span className='author-name'>
                                            {sentenceCase(name)}
                                            {certified && <span className='certified fas fa-badge-check'></span>}
                                        </span>
                                        <span className='author-details'>{details}</span>
                                    </div>
                                    <span className='timestamp'>{formatTimestamp(timestamp)}</span>
                                </div>
                                <p className='answer-content'>{message}</p>
                            </div>
                        ))}
                </div>
            </div>
            <BottomBar />
        </div>
    )
}

export default Question
