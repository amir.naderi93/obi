import React, { useRef, useEffect } from 'react'
import { Doughnut } from 'react-chartjs-2'
import * as amcore from '@amiiir/amcharts4/core'
import * as amcharts from '@amiiir/amcharts4/charts'
import am4themes_animated from '@amiiir/amcharts4/themes/animated'
import './User.scss'
import BottomBar from '../../Components/BottomBar/BottomBar'
import TopBar from '../../Components/TopBar/TopBar'

amcore.useTheme(am4themes_animated)

function User() {
    let pieDiv = useRef(null)
    let lineDiv = useRef(null)

    useEffect(() => {
        // Create chart instance
        let pieChart = amcore.create(pieDiv.current, amcharts.PieChart)

        let pieSeries = pieChart.series.push(new amcharts.PieSeries())
        pieSeries.dataFields.value = 'earnings'
        pieSeries.dataFields.category = 'activity'

        pieChart.innerRadius = amcore.percent(60)

        pieSeries.slices.template.stroke = amcore.color('#fff')
        pieSeries.slices.template.strokeWidth = 2
        pieSeries.slices.template.strokeOpacity = 1
        pieSeries.slices.template.states.getKey('hover').properties.scale = 1
        pieSeries.slices.template.states.getKey('active').properties.shiftRadius = 0

        pieSeries.colors.list = [
            amcore.color('#f44236'),
            amcore.color('#28a745'),
            amcore.color('#ffc200'),
            amcore.color('#70a3ff'),
            amcore.color('#b57edc'),
            amcore.color('#fa9938'),
            amcore.color('#22b8cf')
        ]

        let label = pieSeries.createChild(amcore.Label)
        label.text = '1535'
        label.horizontalCenter = 'middle'
        label.verticalCenter = 'middle'
        label.fontSize = 40

        pieSeries.labels.template.disabled = true

        pieSeries.ticks.template.disabled = true

        // Add a legend
        pieChart.legend = new amcharts.Legend()
        pieChart.legend.position = 'bottom'
        pieChart.legend.padding(0, 0, 0, 0)

        pieChart.data = [
            { activity: 'Questions', earnings: 153 },
            { activity: 'Answers', earnings: 31 },
            { activity: 'Posts', earnings: 154 },
            { activity: 'Engagement', earnings: 1197 }
        ]

        let lineChart = amcore.create(lineDiv.current, amcharts.XYChart)

        // Add data
        lineChart.data = [
            {
                date: '2019-11-10',
                value: 1200
            },
            {
                date: '2019-11-20',
                value: 1215
            },
            {
                date: '2019-12-01',
                value: 1235
            },
            {
                date: '2019-12-10',
                value: 1235
            },
            {
                date: '2019-12-20',
                value: 1235
            },
            {
                date: '2020-01-01',
                value: 1235
            },
            {
                date: '2020-01-10',
                value: 1235
            },
            {
                date: '2020-01-20',
                value: 1235
            },
            {
                date: '2020-02-01',
                value: 1235
            },
            {
                date: '2020-02-10',
                value: 1240
            },
            {
                date: '2020-02-20',
                value: 1250
            },

            {
                date: '2020-03-01',
                value: 1400
            },
            {
                date: '2020-03-10',
                value: 1400
            },
            {
                date: '2020-03-20',
                value: 1400
            },
            {
                date: '2020-04-01',
                value: 1450
            },
            {
                date: '2020-04-10',
                value: 1470
            },
            {
                date: '2020-04-20',
                value: 1475
            },
            {
                date: '2020-05-01',
                value: 1490
            },
            {
                date: '2020-05-10',
                value: 1505
            },
            {
                date: '2020-05-20',
                value: 1535
            },
            {
                date: '2020-05-24',
                value: 1535
            }
        ]

        // Set input format for the dates
        lineChart.dateFormatter.inputDateFormat = 'yyyy-MM-dd'

        lineChart.padding(10, 10, 10, -10)

        // Create axes
        let dateAxis = lineChart.xAxes.push(new amcharts.DateAxis())
        let valueAxis = lineChart.yAxes.push(new amcharts.ValueAxis())

        dateAxis.baseInterval = { timeUnit: 'day', count: 10 }
        dateAxis.startLocation = 0.5
        dateAxis.endLocation = 0.5

        // Create series
        let series = lineChart.series.push(new amcharts.LineSeries())
        series.dataFields.valueY = 'value'
        series.dataFields.dateX = 'date'
        series.tooltipText = '{value}'
        series.strokeWidth = 2
        series.minBulletDistance = 10
    }, [])

    return (
        <div className='page user'>
            <TopBar profile={true} />
            <div className='main'>
                <div className='badge-container'>
                    <span className='badge silver'>Hard worker</span>
                    <span className='badge bronze'>Helpful</span>
                    <span className='badge gold'>Drill Master</span>
                    <span className='badge certified'>Certified Welder</span>
                </div>
                <div className='progress-bar-container'>
                    <div className='progress-bar'></div>
                    <div className='overlay'>
                        <span className='progress-title'>NEXT GOAL: </span>
                        <span className='progress-details'>
                            Answer two more questions to achieve <span className='badge'>Handy man</span> badge
                        </span>
                    </div>
                </div>
                <div ref={pieDiv} className='chart-container'></div>
                <div ref={lineDiv} className='chart-container'></div>
            </div>
            <BottomBar />
        </div>
    )
}

export default User
