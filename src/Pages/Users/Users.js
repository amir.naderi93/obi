import React from 'react'
import { sentenceCase } from 'change-case'
import './Users.scss'
import TopBar from '../../Components/TopBar/TopBar'
import BottomBar from '../../Components/BottomBar/BottomBar'
import { users } from '../../constants'

function Users() {
    return (
        <div className='page users'>
            <TopBar />
            <div className='leader-board-title'>LEADER BOARD</div>
            <div className='main'>
                {users
                    .slice(0)
                    .sort((a, b) => b.score - a.score)
                    .map(({ name, details, avatar, certified, score }, i) => (
                        <div key={i} className='user-container' onClick={() => (window.location = '/user')}>
                            <img src={avatar} alt={name}></img>
                            <span className='user-name'>
                                {sentenceCase(name)}
                                {certified && <span className='certified fas fa-badge-check'></span>}
                            </span>
                            <span className='user-details'>{details}</span>
                            <span className='user-details'>
                                {score}
                                <span className='far fa-gem'></span>
                            </span>
                        </div>
                    ))}
            </div>
            <BottomBar />
        </div>
    )
}

export default Users
